﻿using System;

namespace Bowlingkata
{
    public class Game
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        int[] pinFalls = new int[21];
        int currentBall = 0;
        int frameIndex = 0;
        public void Roll(int pins)
        {
            pinFalls[currentBall] = pins;
            currentBall++;
        }

        public int StrikeBonus()
        {
            return pinFalls[frameIndex+1] + pinFalls[frameIndex + 2];
        }

        public bool IsSpare()
        {
            return pinFalls[frameIndex] + pinFalls[frameIndex + 1] == 10;
        }

        public int Score()
        {
            int score = 0;

            for (int frame = 0; frame < 10;frame++)
            {
                if (IsSpare())
                {
                    score += 10 + pinFalls[frameIndex + 2];
                    frameIndex += 2;
                }
                else if (pinFalls[frameIndex]==10)
                {
                    score += 10 + StrikeBonus();
                    frameIndex += 1;
                }
                else
                {
                    score += pinFalls[frameIndex] + pinFalls[frameIndex + 1];
                    frameIndex += 2;
                }
            }
            
            return score;
        }
    }
}
