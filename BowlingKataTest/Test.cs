﻿using NUnit.Framework;
using System;
using Bowlingkata;
namespace BowlingKataTest
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }

        void RollMany(int balls, int pins)
        {
            for (int i = 0; i < balls; i++)
            {
                game.Roll(pins);
            }
        }

        [Test]
        public void GutterGame()
        {
            for (int i = 0; i < 20; i++)
            {
                game.Roll(0);
            }

            Assert.That(game.Score, Is.EqualTo(0));
        }

        [Test]
        public void AllOnes()
        {
            for (int i = 0; i < 20; i++)
            {
                game.Roll(1);
            }

            Assert.That(game.Score, Is.EqualTo(20));
        }

        [Test]
        public void OneSpareInFirstFrame()
        {
            game.Roll(9);
            game.Roll(1);
            RollMany(18, 1);

            Assert.That(game.Score, Is.EqualTo(29));
        }

        [Test]
        public void OneStrikeInFirstFrame()
        {
            game.Roll(10);
            RollMany(19, 1);
            Assert.That(game.Score, Is.EqualTo(30));
        }

        [Test]
        public void PerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score, Is.EqualTo(300));
        }

        [Test]
        public void OneStrikeAtLastFrame()
        {
            RollMany(18, 1);
            RollMany(3, 10);
            Assert.That(game.Score, Is.EqualTo(48));
        }

        [Test]
        public void OneSpareInLastFrame()
        {
            RollMany(18, 1);
            RollMany(3, 5);
            Assert.That(game.Score, Is.EqualTo(33));
        }

        [Test]
public void AllSpare()
        {
            RollMany(21, 5);
            Assert.That(game.Score, Is.EqualTo(150));
        }
    }
}
